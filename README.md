# Apollo-fullstack-basics

- Api that uses GraphQl to query and mutate data
- This is the fullstack app for the [Apollo tutorial](http://apollographql.com/docs/tutorial/introduction.html). 🚀

![alt text](https://apollographql.gallerycdn.vsassets.io/extensions/apollographql/vscode-apollo/1.17.0/1600754574150/Microsoft.VisualStudio.Services.Icons.Default)

## Objectives:

- This is the first project made with Apollo/GraphQl to learn the conecpts of the tecnology

## What does it do?

- This uses the [SpaceX](https://api.spacexdata.com/v2/) api to query, list, book and cancel trips by an authenticated user

## File structure

The app is split out into two folders:

- `start`: Starting point for the tutorial
- `final`: Final version

From within the `start` and `final` directories, there are two folders (one for `server` and one for `client`).

## Installation

To run the app, run these commands in two separate terminal windows from the root:

```bash
cd final/server && npm i && npm start
```

and

```bash
cd final/client && npm i && npm start
```
